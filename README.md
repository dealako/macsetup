# Mac Development Machine Setup

## Prerequisites

* Python
* Brew
* Ansible

## Installation

This is a [Ansible](https://www.ansible.com/) playbook to quickly setup a Mac
based on my preferences.

To execute the ansible playbook, run the following command:

```bash
ansible-playbook -i ./hosts playbook.yml --verbose
```

The playbook uses homebrew with cask to install most things. You can see a
list of what get's installed at in the `roles/setup/vars/main.yml` file.

The applications that are not traditionally installed via `brew` can be
automatically downloaded to the `$HOME/Downloads` folder. From there, simply
run the installation manually.

## References

* Playbook examples derived from [this repository](https://github.com/daemonza/setupmac)
* [About Ansible](https://www.ansible.com/resources/videos/quick-start-video)
